package models;

public class Rectangle extends Shape{
     protected double length = 1.0;
     protected double width  = 1.0;


     public Rectangle() {
     }


     public Rectangle(double length, double width) {
          this.length = length;
          this.width = width;
     }


     public Rectangle(String color, boolean filled, double length, double width) {
          super(color, filled);
          this.length = length;
          this.width = width;
     }


     public double getLength() {
          return length;
     }


     public void setLength(double length) {
          this.length = length;
     }


     public double getWidth() {
          return width;
     }


     public void setWidth(double width) {
          this.width = width;
     }

     @Override
     public double getArea(){
          return this.width * this.length;
     }

     @Override
     public double getPerimeter(){
          return (this.length + this.width) * 2;
     }


     @Override
     public String toString() {
          return "Rectangle [" + super.toString() + ", length=" + this.length + ", width=" + this.width + "]";
     }
}
