import models.Circle;
import models.Rectangle;
import models.Square;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle = new Rectangle("blue", false, 20, 10);
        System.out.println("Rectangle: ");
        System.out.println(rectangle.getArea());
        System.out.println(rectangle.getPerimeter());
        System.out.println(rectangle.toString());
        System.out.println("----------------------------------------------");

        Circle circle = new Circle(2.5, "Red", false);
        System.out.println("Circle: ");
        System.out.println(circle.getArea());
        System.out.println(circle.getPerimeter());
        System.out.println(circle.toString());
        System.out.println("----------------------------------------------");

        Square square = new Square("green", false, 4);
        System.out.println("Square: ");
        System.out.println(square.getArea());
        System.out.println(square.getPerimeter());
        System.out.println(square.toString());
        System.out.println("----------------------------------------------");
    }
}
